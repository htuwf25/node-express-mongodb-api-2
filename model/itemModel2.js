const mongoose = require('mongoose');

const itemSchema = new mongoose.Schema({
  description: String,
});

module.exports = mongoose.model('ItemAboutMeData', itemSchema);