const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const { SecretManagerServiceClient } = require('@google-cloud/secret-manager');

const app = express();
const port = process.env.PORT || 3000;
const cors = require('cors');

app.use(bodyParser.urlencoded({ extended: false }), bodyParser.json());
const corsOptions = {
  origin: '*',
  methods: ['GET', 'POST', 'PUT', 'DELETE'],
  allowedHeaders: ['Content-Type', 'Authorization']
};
app.use(cors(corsOptions));

async function getMongoDBSecret() {
  try {
    const client = new SecretManagerServiceClient();
    const [version] = await client.accessSecretVersion({
      name: 'projects/1072257678284/secrets/db/versions/1',
    });
    return version.payload.data.toString('utf8');
  } catch (err) {
    console.error('Error retrieving MongoDB secret:', err);
    return null;
  }
}

async function startServer() {
  try {
    const mongodbUri = await getMongoDBSecret();
    if (!mongodbUri) {
      console.error('MongoDB URI not found in secrets.');
      return;
    }

    await mongoose.connect(mongodbUri, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    });

    console.log('Successfully Connected to MongoDB');

    const itemRoutes = require('./routes/itemRoutes');
    app.use('/node-api', itemRoutes);

    app.listen(port, () => {
      console.log(`Server is running on port ${port}`);
    });
  } catch (err) {
    console.error('Error starting server:', err);
  }
}

startServer();
