const Item = require('../model/itemModel');
const ItemAboutMeData = require('../model/itemModel2');


exports.getAllItems = async (req, res) => {
  try {
    const items = await Item.find();
    res.status(200).json(items);
  } catch (err) {
    res.status(500).json({ error: 'Failed to fetch items' });
  }
};

//aboutme data

exports.getAllItemAboutMeData = async (req, res) => {
  try {
    const items = await ItemAboutMeData.find();
    res.status(200).json(items);
  } catch (err) {
    res.status(500).json({ error: 'Failed to fetch items' });
  }
};